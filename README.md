To build:
```
./gradlew build
```
To run:
```
java -jar build/libs/set-storage-0.0.1-SNAPSHOT.jar
```
application will listen to 8080 port by default.
Endpoints:

* Upload set `/stringSets`:
```
POST http://localhost:8080/stringSets
Content-Type: application/json

["abc", "cde"]
```
Result:
```
{
  "id": "6b9fd066-f6f7-4e87-a69d-1d244478d986",
  "set": [
    "abc",
    "cde"
  ]
}
```
* Search (find all the sets containing given string) `/stringSets?searchString=foo`
```
GET http://localhost:8080/stringSets?searchString=a
```
Result:
```
[
  "b98254aa-80f7-486d-b98b-59e6c6a3d9d8"
]
```
* Delete set `/stringSets/{id}`
```
DELETE http://localhost:8080/b98254aa-80f7-486d-b98b-59e6c6a3d9d8
```
Result:
```
<Response is empty>
```
* Statistic `/stringSets/{id}/statistic`
```
GET http://localhost:8080/stringSets/b360556c-e174-41b0-b66c-ebb26123472c/statistic
```
Result:
```
{
  "numberOfStrings": 3,
  "shortestStringLength": 1,
  "longestStringLength": 2,
  "averageStringLength": 1.3333333333333333,
  "medianStringLength": 1.0
}
```
Following 3 endpoints all return an array of strings via a GET method.

* Most common string `/strings/mostCommon`

* Longest string `/strings/longest`

* Exactly in `/strings?exactlyIn=4`

* Intersection `/stringSets/intersection?left={leftId}&right={rightId}`
```
POST http://localhost:8080/stringSets/intersections?left=534073ad-6b55-43f6-9c66-ffcf80aa3973&right=067c0b58-9f55-4683-9066-d454a660c766
```
Result:
```
{
  "id": "51580cba-20f1-4e1b-8300-a12dd69a4f59",
  "set": [
    "ee",
    "abc",
    "cf"
  ]
}
```
* Longest chain of strings: `/strings/longestChain`
```
GET http://localhost:8080/stringSets/longestChain
```
Result:
```
[
  "abc",
  "cde",
  "e"
]
```
