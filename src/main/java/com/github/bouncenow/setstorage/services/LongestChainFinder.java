package com.github.bouncenow.setstorage.services;

import com.github.bouncenow.setstorage.model.StringSet;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
class LongestChainFinder {

    private final Collection<StringSet> sets;

    List<String> findLongestChain() {

        if (sets.isEmpty()) {
            return Collections.emptyList();
        }

        List<ChainsWithIndices> longestChainsForSingleSets = sets.stream()
                .map(s -> ChainsWithIndices.of(findAllChainsOfSet(s)))
                .collect(Collectors.toList());

        Chain longest = getLongestBetweenEveryCombinationOfTwoSets(longestChainsForSingleSets);

        return longest.asStrings();
    }

    private Chain getLongestBetweenEveryCombinationOfTwoSets(List<ChainsWithIndices> longestChains) {
        Chain longest = longestChains.get(0).getChains().stream()
                .max(Comparator.comparing(Chain::length))
                .get();
        for (int i = 0; i < sets.size(); i++) {
            for (int j = i + 1; j < sets.size(); j++) {
                Chain longestFromTo = findLongestChainBetweenTwoSets(longestChains.get(i), longestChains.get(j));
                if (longestFromTo.length() > longest.length()) {
                    longest = longestFromTo;
                }
                Chain longestToFrom = findLongestChainBetweenTwoSets(longestChains.get(j), longestChains.get(i));
                if (longestToFrom.length() > longest.length()) {
                    longest = longestToFrom;
                }
            }
        }
        return longest;
    }

    private Chain findLongestChainBetweenTwoSets(ChainsWithIndices from, ChainsWithIndices to) {
        return Stream
                .concat(
                        from.getChains().stream(),
                        from.getChains().stream()
                                .flatMap(c -> nextChainsBetweenTwoSets(c, to.getChainsByFirstChar()).stream())
                )
                .max(Comparator.comparing(Chain::length))
                .orElseThrow(() -> new IllegalStateException("These lists should not be empty"));
    }

    private List<Chain> findAllChainsOfSet(StringSet set) {
        Map<Character, Set<String>> stringsByFirstChar = set
                .getSet().stream()
                .collect(Collectors.groupingBy(s -> s.charAt(0), Collectors.toSet()));

        List<Chain> allChainsOfSet = new ArrayList<>();

        List<Chain> currents = set
                .getSet().stream()
                .map(Chain::ofSingle)
                .collect(Collectors.toList());
        while (!currents.isEmpty()) {
            allChainsOfSet.addAll(currents);
            currents = currents.stream()
                    .flatMap(seq -> nextChainsForSingleSet(seq, stringsByFirstChar).stream())
                    .collect(Collectors.toList());
        }

        return allChainsOfSet;
    }

    private List<Chain> nextChainsForSingleSet(Chain current, Map<Character, Set<String>> stringsByFirstChar) {
        Set<String> connected = stringsByFirstChar.get(current.last());
        if (connected == null) {
            return Collections.emptyList();
        }
        return connected.stream()
                .filter(current::notVisited)
                .map(current::withNext)
                .collect(Collectors.toList());
    }

    private List<Chain> nextChainsBetweenTwoSets(Chain current, Map<Character, Set<Chain>> stringsByFirstChar) {
        Set<Chain> connected = stringsByFirstChar.get(current.last());
        if (connected == null) {
            return Collections.emptyList();
        }
        return connected.stream()
                .map(current::withNext)
                .collect(Collectors.toList());
    }
}

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
class Chain {

    private final LinkedHashSet<String> strings;
    private final char first;
    private final char last;

    static Chain ofSingle(String s) {
        LinkedHashSet<String> strings = new LinkedHashSet<>();
        strings.add(s);
        return new Chain(strings, s.charAt(0), s.charAt(s.length() - 1));
    }

    boolean notVisited(String s) {
        return !strings.contains(s);
    }

    Chain withNext(String s) {
        LinkedHashSet<String> withNext = new LinkedHashSet<>(strings);
        withNext.add(s);
        return new Chain(withNext, first, s.charAt(s.length() - 1));
    }

    Chain withNext(Chain c) {
        LinkedHashSet<String> withNext = new LinkedHashSet<>(strings);
        withNext.addAll(c.strings);
        return new Chain(withNext, first, c.last);
    }

    char first() {
        return first;
    }

    char last() {
        return last;
    }

    int length() {
        return strings.size();
    }

    List<String> asStrings() {
        return new ArrayList<>(strings);
    }

}

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
class ChainsWithIndices {
    private final List<Chain> chains;
    private final Map<Character, Set<Chain>> chainsByFirstChar;

    static ChainsWithIndices of(List<Chain> chains) {
        Map<Character, Set<Chain>> chainsByFirst = chains.stream()
                .collect(Collectors.groupingBy(Chain::first, Collectors.toSet()));

        return new ChainsWithIndices(chains, chainsByFirst);
    }
}

