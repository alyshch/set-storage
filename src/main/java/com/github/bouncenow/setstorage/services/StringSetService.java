package com.github.bouncenow.setstorage.services;

import com.github.bouncenow.setstorage.exceptions.InvalidSetException;
import com.github.bouncenow.setstorage.model.SetStatistic;
import com.github.bouncenow.setstorage.model.StringSet;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

@Service
public class StringSetService {

    private final Map<UUID, StringSet> setsById = new HashMap<>();

    private final Map<String, Set<UUID>> setIdsByStrings = new HashMap<>();

    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    public StringSet create(List<String> strings) {
        if (strings.isEmpty()) {
            throw new InvalidSetException("Set should not be empty");
        }

        if (!allUnique(strings)) {
            throw new InvalidSetException("Set should not have duplicate strings");
        }

        try {
            lock.writeLock().lock();

            return createAndUpdateIndices(strings);
        } finally {
            lock.writeLock().unlock();
        }
    }

    public Optional<StringSet> get(UUID id) {
        try {
            lock.readLock().lock();

            return Optional.ofNullable(setsById.get(id));
        } finally {
            lock.readLock().unlock();
        }
    }

    public boolean delete(UUID id) {
        try {
            lock.writeLock().lock();

            return deleteAndUpdateIndices(id);
        } finally {
            lock.writeLock().unlock();
        }
    }

    public Set<UUID> search(String searchString) {
        try {
            lock.readLock().lock();

            return getSetIdsByIndex(searchString);
        } finally {
            lock.readLock().unlock();
        }
    }

    public Optional<SetStatistic> statistic(UUID id) {
        try {
            lock.readLock().lock();

            return getSetStatisticIfExists(id);
        } finally {
            lock.readLock().unlock();
        }
    }

    public List<String> mostCommonString() {
        try {
            lock.readLock().lock();

            return mostCommonStringUsingIndex();
        } finally {
            lock.readLock().unlock();
        }
    }

    public List<String> longestString() {
        try {
            lock.readLock().lock();

            return longestStringHelper();
        } finally {
            lock.readLock().unlock();
        }
    }

    public List<String> stringsExactlyInGivenNumberOfSets(int numberOfSets) {
        try {
            lock.readLock().lock();

            return getStringsThatAreInGivenNumberOfSetsSortedAlphabetically(numberOfSets);
        } finally {
            lock.readLock().unlock();
        }
    }

    public Optional<StringSet> intersection(UUID leftId, UUID rightId) {
        try {
            lock.writeLock().lock();

            return intersectionHelper(leftId, rightId);
        } finally {
            lock.writeLock().unlock();
        }
    }

    public List<String> longestChain() {
        try {
            lock.readLock().lock();

            return new LongestChainFinder(setsById.values()).findLongestChain();
        } finally {
            lock.readLock().unlock();
        }
    }

    private Optional<StringSet> intersectionHelper(UUID leftId, UUID rightId) {
        StringSet left = setsById.get(leftId);
        if (left == null) {
            return Optional.empty();
        }

        StringSet right = setsById.get(rightId);
        if (right == null) {
            return Optional.empty();
        }

        List<String> intersectionSet = left.getSet().stream()
                .filter(s -> right.getSet().contains(s))
                .collect(Collectors.toList());

        return Optional.of(StringSet.create(intersectionSet));
    }

    private List<String> longestStringHelper() {
        return setIdsByStrings
                .keySet()
                .stream()
                .map(String::length)
                .max(Comparator.naturalOrder())
                .map(maxLength -> setIdsByStrings
                        .keySet()
                        .stream()
                        .filter(s -> s.length() == maxLength)
                        .sorted()
                        .collect(Collectors.toList()))
                .orElse(Collections.emptyList());
    }

    private List<String> mostCommonStringUsingIndex() {
        return setIdsByStrings
                .entrySet()
                .stream()
                .map(e -> e.getValue().size())
                .max(Comparator.naturalOrder())
                .map(this::getStringsThatAreInGivenNumberOfSetsSortedAlphabetically)
                .orElse(Collections.emptyList());
    }

    private List<String> getStringsThatAreInGivenNumberOfSetsSortedAlphabetically(int numberOfSets) {
        return setIdsByStrings.entrySet().stream()
                .filter(e -> e.getValue().size() == numberOfSets)
                .map(Map.Entry::getKey)
                .sorted()
                .collect(Collectors.toList());
    }

    private Optional<SetStatistic> getSetStatisticIfExists(UUID id) {
        return Optional
                .ofNullable(setsById.get(id))
                .map(SetStatistic::fromSet);
    }

    private Set<UUID> getSetIdsByIndex(String searchString) {
        Set<UUID> setIds = setIdsByStrings.get(searchString);
        if (setIds != null) {
            return setIds;
        } else {
            return Collections.emptySet();
        }
    }

    private boolean deleteAndUpdateIndices(UUID id) {
        StringSet toRemove = setsById.remove(id);

        if (toRemove == null) {
            return false;
        }

        deleteFromIndex(toRemove);

        return true;
    }

    private void deleteFromIndex(StringSet toRemove) {
        for (String s : toRemove.getSet()) {
            Set<UUID> setIds = setIdsByStrings.get(s);
            setIds.remove(toRemove.getId());
            if (setIds.isEmpty()) {
                setIdsByStrings.remove(s);
            }
        }
    }

    private StringSet createAndUpdateIndices(List<String> strings) {
        StringSet set = StringSet.create(strings);
        setsById.put(set.getId(), set);

        updateIndexByString(strings, set);

        return set;
    }

    private void updateIndexByString(List<String> strings, StringSet set) {
        for (String s : strings) {
            setIdsByStrings.computeIfAbsent(s, str -> new HashSet<>()).add(set.getId());
        }
    }

    private boolean allUnique(List<String> strings) {
        Set<String> seen = new HashSet<>(strings.size());

        for (String s : strings) {
            if (!seen.add(s)) {
                return false;
            }
        }

        return true;
    }

}
