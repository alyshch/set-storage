package com.github.bouncenow.setstorage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SetStorageApplication {

	public static void main(String[] args) {
		SpringApplication.run(SetStorageApplication.class, args);
	}
}
