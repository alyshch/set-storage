package com.github.bouncenow.setstorage.controllers;

import com.github.bouncenow.setstorage.services.StringSetService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/strings")
@RequiredArgsConstructor
public class StringsController {

    private final StringSetService stringSetService;

    @GetMapping("/mostCommon")
    public List<String> mostCommon() {
        return stringSetService.mostCommonString();
    }

    @GetMapping("/longest")
    public List<String> longest() {
        return stringSetService.longestString();
    }

    @GetMapping
    public List<String> stringsExactlyInGivenNumberOfSets(@RequestParam("exactlyIn") int numberOfSets) {
        return stringSetService.stringsExactlyInGivenNumberOfSets(numberOfSets);
    }

}
