package com.github.bouncenow.setstorage.controllers;

import com.github.bouncenow.setstorage.model.SetStatistic;
import com.github.bouncenow.setstorage.model.StringSet;
import com.github.bouncenow.setstorage.services.StringSetService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;
import java.util.UUID;

@RestController
@RequestMapping("/stringSets")
@RequiredArgsConstructor
public class StringSetsController {

    private final StringSetService stringSetService;

    @PostMapping
    public StringSet create(@RequestBody List<String> strings) {
        return stringSetService.create(strings);
    }

    @GetMapping("/{id}")
    public ResponseEntity<StringSet> get(@PathVariable UUID id) {
        return ResponseEntity.of(stringSetService.get(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable UUID id) {
        boolean removed = stringSetService.delete(id);

        if (removed) {
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping
    public Set<UUID> search(@RequestParam String searchString) {
        return stringSetService.search(searchString);
    }

    @GetMapping("/{id}/statistic")
    public ResponseEntity<SetStatistic> statistic(@PathVariable UUID id) {
        return ResponseEntity.of(stringSetService.statistic(id));
    }

    @PostMapping("/intersections")
    public ResponseEntity<StringSet> intersection(@RequestParam UUID left, @RequestParam UUID right) {
        return ResponseEntity.of(stringSetService.intersection(left, right));
    }

    @GetMapping("/longestChain")
    public List<String> longestChain() {
        return stringSetService.longestChain();
    }

}
