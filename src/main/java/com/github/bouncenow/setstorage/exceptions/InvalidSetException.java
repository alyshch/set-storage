package com.github.bouncenow.setstorage.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidSetException extends RuntimeException {

    public InvalidSetException(String message) {
        super(message);
    }

}
