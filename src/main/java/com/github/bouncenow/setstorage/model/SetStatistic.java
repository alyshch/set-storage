package com.github.bouncenow.setstorage.model;

import lombok.Data;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class SetStatistic {

    private int numberOfStrings;

    private int shortestStringLength;

    private int longestStringLength;

    private double averageStringLength;

    private double medianStringLength;

    public static SetStatistic fromSet(StringSet set) {
        SetStatistic statistic = new SetStatistic();

        List<Integer> lengths = set.getSet().stream()
                .map(String::length)
                .collect(Collectors.toList());

        statistic.numberOfStrings = set.getSet().size();
        statistic.shortestStringLength = Collections.min(lengths);
        statistic.longestStringLength = Collections.max(lengths);
        statistic.averageStringLength = average(lengths);
        statistic.medianStringLength = median(lengths);

        return statistic;
    }

    private static double average(List<Integer> values) {
        return values
                .stream()
                .reduce((a, b) -> a + b)
                .map(lengthSum -> lengthSum * 1.0 / values.size())
                .orElseThrow(() -> new IllegalStateException("Set should not be empty"));
    }

    private static double median(List<Integer> values) {
        Collections.sort(values);

        int middle = values.size() / 2;

        if (values.size() % 2 == 0) {
            return (values.get(middle - 1) + values.get(middle)) / 2.0;
        } else {
            return values.get(middle);
        }
    }

}
