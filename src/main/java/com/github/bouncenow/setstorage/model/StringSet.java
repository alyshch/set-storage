package com.github.bouncenow.setstorage.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
public class StringSet {

    private final UUID id;

    private final Set<String> set;

    public static StringSet create(List<String> strings) {
        Set<String> asSet = new HashSet<>(strings);
        return new StringSet(UUID.randomUUID(), asSet);
    }
}
