package com.github.bouncenow.setstorage.services;

import com.github.bouncenow.setstorage.exceptions.InvalidSetException;
import com.github.bouncenow.setstorage.model.SetStatistic;
import com.github.bouncenow.setstorage.model.StringSet;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class StringSetServiceTest {

    private StringSetService stringSetService;

    @Before
    public void setUp() {
        stringSetService = new StringSetService();
    }

    @Test
    public void testIllegalSetCreationWhenThereAreDuplicates() {
        assertThrows(InvalidSetException.class, () -> stringSetService.create(Arrays.asList("a", "a")));
    }

    @Test
    public void testIllegalSetCreationWhenSetIsEmpty() {
        assertThrows(InvalidSetException.class, () -> stringSetService.create(Collections.emptyList()));
    }

    @Test
    public void testSuccessfulSetCreation() {
        StringSet set = createSet("a", "b", "c");

        assertEquals(new HashSet<>(Arrays.asList("a", "b", "c")), set.getSet());
    }

    @Test
    public void testSuccessfulGet() {
        StringSet set = createSet("a", "b", "c");

        assertEquals(Optional.of(set), stringSetService.get(set.getId()));
    }

    @Test
    public void testNonSuccessfulGet() {
        assertEquals(Optional.empty(), stringSetService.get(UUID.randomUUID()));
    }

    @Test
    public void testSuccessfulDelete() {
        StringSet set = createSet("a", "b", "c");

        assertTrue(stringSetService.delete(set.getId()));
        assertEquals(Optional.empty(), stringSetService.get(set.getId()));
    }

    @Test
    public void testNonSuccessfulDelete() {
        assertFalse(stringSetService.delete(UUID.randomUUID()));
    }

    @Test
    public void testSearch() {
        StringSet set1 = createSet("a", "b");

        StringSet set2 = createSet("b");

        Set<UUID> searchForA = stringSetService.search("a");
        assertEquals(Collections.singleton(set1.getId()), searchForA);

        Set<UUID> searchForB = stringSetService.search("b");
        assertEquals(new HashSet<>(Arrays.asList(set1.getId(), set2.getId())), searchForB);
    }

    @Test
    public void testStatisticForNonExistentSet() {
        assertEquals(Optional.empty(), stringSetService.statistic(UUID.randomUUID()));
    }

    @Test
    public void testStatistic() {
        StringSet set = createSet("a", "b", "c");

        assertEquals(Optional.of(SetStatistic.fromSet(set)), stringSetService.statistic(set.getId()));
    }

    @Test
    public void testMostCommon() {
        assertTrue(stringSetService.mostCommonString().isEmpty());

        createSet("a", "b", "c");

        createSet("a", "b", "c");

        createSet("a", "b", "g");

        assertEquals(Arrays.asList("a", "b"), stringSetService.mostCommonString());
    }

    @Test
    public void testLongest() {
        assertTrue(stringSetService.longestString().isEmpty());

        createSet("a", "bc", "cdefghi");

        createSet("fghi", "jklmopq", "r");

        assertEquals(Arrays.asList("cdefghi", "jklmopq"), stringSetService.longestString());
    }

    @Test
    public void testExactlyIn() {
        createSet("a", "bc", "cdefghi");

        createSet("a", "bc", "r");

        createSet("gh", "bc", "r");

        assertEquals(Arrays.asList("a", "r"), stringSetService.stringsExactlyInGivenNumberOfSets(2));
        assertEquals(Collections.singletonList("bc"), stringSetService.stringsExactlyInGivenNumberOfSets(3));
    }

    @Test
    public void testIntersection() {
        StringSet set1 = createSet("a", "b", "c");
        StringSet set2 = createSet("b", "c", "e");

        Optional<StringSet> intersection = stringSetService.intersection(set1.getId(), set2.getId());
        assertTrue(intersection.isPresent());
        assertEquals(new HashSet<>(Arrays.asList("b", "c")), intersection.get().getSet());
    }

    @Test
    public void testLongestChain() {
        createSet("foo", "ooomph", "hgh");
        createSet("hij", "jkl", "jkm", "lmn");
        createSet("abc", "cde", "cdf", "fuf", "fgh");

        assertEquals(Arrays.asList("abc", "cdf", "fuf", "fgh", "hij", "jkl", "lmn"), stringSetService.longestChain());
    }

    @Test
    public void testLongestChainSingle() {
        createSet("foo");

        assertEquals(Collections.singletonList("foo"), stringSetService.longestChain());
    }


    private StringSet createSet(String... strings) {
        return stringSetService.create(Arrays.asList(strings));
    }
}